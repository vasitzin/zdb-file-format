#pragma once
#ifndef ZDB_HEADER_LIBRARY
#define ZDB_HEADER_LIBRARY

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <vector>
#include <filesystem>
#include <random>

#define N_MAGIC  -601209
#define N_UUID   37
#define N_TITLE  24
#define N_HEADER 32
#define N_VALUE  32

namespace fs = std::filesystem;

typedef const int zdb_magic_t;
typedef std::string zdb_uuid_t;
typedef std::string zdb_title_t;
typedef std::string zdb_header_t;
typedef std::vector<zdb_header_t> zdb_headers_t;
typedef std::string zdb_value_t;
typedef std::vector<zdb_value_t> zdb_entry_t;
typedef std::vector<zdb_entry_t> zdb_entries_t;

static zdb_magic_t magic_num{N_MAGIC};
enum zdb_state_t { safe, fresh, edited };

class ZDB {
private:
  zdb_uuid_t uuid;
  zdb_title_t title;
  zdb_headers_t headers;
  zdb_entries_t entries;
  zdb_state_t state;

  static zdb_uuid_t generate_uuid() {
    const char hex_symbols[]{"0123456789abcdef"};
    const bool layout[]{0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0};
    std::random_device rd;
    std::mt19937_64 rng{rd()};
    std::uniform_int_distribution<int> dist(0, 15);
    zdb_uuid_t uuid;
    for (int i{0}; i < 16; ++i) {
      if (layout[i]) uuid += "-";
      uuid += hex_symbols[dist(rng)];
      uuid += hex_symbols[dist(rng)];
    }
    return uuid;
  }

  ZDB(){}; // Empty Constructor, only for inside use!

public:
  /**
   * Fresh Constructor, used for newly created Tables.
   */
  ZDB(zdb_title_t &title, zdb_headers_t &headers) {
    this->uuid = generate_uuid();
    this->title = title;
    this->headers = headers;
    this->entries = {};
    this->state = fresh;
  };

  /**
   * Load Constructor, load specified object from disk.
   */
  ZDB(std::ifstream &file) {
    file.ignore(sizeof(int)); // skip magic number

    zdb_uuid_t uuid;
    char uuid_c[N_UUID];
    file.read((char *)&uuid_c, sizeof(char[N_UUID]));
    uuid = uuid_c;

    zdb_title_t title;
    char title_c[N_TITLE];
    file.read((char *)&title_c, sizeof(char[N_TITLE]));
    title = title_c;

    size_t headers_size;
    file.read((char *)&headers_size, sizeof(size_t));
    size_t entries_size;
    file.read((char *)&entries_size, sizeof(size_t));

    zdb_headers_t headers{};
    char header_c[N_HEADER];
    for (size_t col{0}; col < headers_size; ++col) {
      file.read((char *)&header_c, sizeof(char[N_HEADER]));
      std::string header{header_c};
      headers.push_back(header);
    }

    zdb_entries_t entries{};
    char value_c[N_VALUE];
    for (size_t row{0}; row < entries_size; ++row) {
      zdb_entry_t entry{};
      for (size_t col{0}; col < headers_size; ++col) {
        file.read((char *)&value_c, sizeof(char[N_VALUE]));
        std::string value{value_c};
        entry.push_back(value);
      }
      entries.push_back(entry);
    }

    this->uuid = uuid;
    this->title = title;
    this->headers = headers;
    this->entries = entries;
    this->state = safe;
  };

  inline zdb_uuid_t get_uuid() { return uuid; };

  inline zdb_title_t get_title() { return title; };

  inline zdb_headers_t get_headers() { return headers; };

  inline zdb_entries_t get_entries() { return entries; };

  inline zdb_state_t get_state() { return state; };

  void set_title(zdb_title_t &new_title) {
    this->title = new_title;
    if (this->state != fresh) this->state = edited;
  };

  void add_header(zdb_header_t &header, const zdb_value_t init_value = "") {
    this->headers.push_back(header);
    for (size_t row{0}; row < entries.size(); ++row) this->entries.at(row).push_back(init_value);
    if (this->state != fresh) this->state = edited;
  };

  void set_header(zdb_header_t &new_header, size_t &col) {
    this->headers.at(col) = new_header;
    if (this->state != fresh) this->state = edited;
  };

  void remove_header(size_t &col) {
    if (headers.size() < 2) {
      std::cout << "[ZDB] At least one header is needed for your Table." << std::endl;
      return;
    }
    for (size_t row{0}; row < entries.size(); ++row)
      entries.at(row).erase(entries.at(row).begin() + col);
    headers.erase(headers.begin() + col);
    if (this->state != fresh) this->state = edited;
  };

  void add_entry(zdb_entry_t &entry) {
    entries.push_back(entry);
    if (this->state != fresh) this->state = edited;
  };

  void set_value(zdb_value_t &new_value, size_t &row, size_t &col) {
    entries.at(row).at(col) = new_value;
    if (this->state != fresh) this->state = edited;
  };

  void move_entry(size_t &dest, size_t &src) {
    if (dest == src) {
      std::cout << "[ZDB] Indeces provided in move_entry() are equal." << std::endl;
      return;
    }
    if (dest < 0 || dest >= this->entries.size() || src < 0 || src >= this->entries.size()) {
      std::cout << "[ZDB] Indeces provided in move_entry() are out of bounds!" << std::endl;
      return;
    }

    if (dest == src - 1 || dest == src + 1) {
      std::swap(this->entries.at(dest), this->entries.at(src));
    } else if (dest == 0) {
      zdb_entry_t entry{this->entries.at(src)};
      this->entries.erase(entries.begin() + src);
      this->entries.insert(this->entries.begin(), 1, entry);
    } else if (dest == this->entries.size() - 1) {
      zdb_entry_t entry{this->entries.at(src)};
      this->entries.erase(entries.begin() + src);
      this->entries.push_back(entry);
    } else {
      // zdb_entry_t entry{this->entries.at(src)};
      // this->entries.erase(entries.begin() + src);
      // if (dest > src) --dest;
      // this->entries.insert(this->entries.begin() + dest, 1, entry);
      std::cout << "[ZDB] The destination index provided in move_entry() must be 0, N, src - 1 or "
                   "src + 1."
                << std::endl;
    }
    if (this->state != fresh) this->state = edited;
  };

  void remove_entry(size_t &row) {
    entries.erase(entries.begin() + row);
    if (this->state != fresh) this->state = edited;
  };

  /**
   * Save to disk.
   */
  void save(std::ofstream &file) {
    file.write((char *)&magic_num, sizeof(int));

    char uuid_c[N_UUID];
    strcpy(uuid_c, uuid.c_str());
    file.write((char *)&uuid_c, sizeof(char[N_UUID]));

    char title_c[N_TITLE];
    strcpy(title_c, title.c_str());
    file.write((char *)&title_c, sizeof(char[N_TITLE]));

    size_t headers_size{headers.size()};
    file.write((char *)&headers_size, sizeof(size_t));
    size_t entries_size{entries.size()};
    file.write((char *)&entries_size, sizeof(size_t));

    char header_c[N_HEADER];
    for (size_t col{0}; col < headers_size; ++col) {
      strcpy(header_c, headers.at(col).c_str());
      file.write((char *)&header_c, sizeof(char[N_HEADER]));
    }

    char value_c[N_VALUE];
    for (size_t row{0}; row < entries_size; ++row) {
      for (size_t col{0}; col < headers_size; ++col) {
        strcpy(value_c, entries.at(row).at(col).c_str());
        file.write((char *)&value_c, sizeof(char[N_VALUE]));
      }
    }

    this->state = safe;
  };

  /**
   * Check if fs entry is a zdb formatted file.
   */
  static bool is_zdb(fs::directory_entry &entry) {
    if (entry.is_directory()) return false;
    int file_number;
    std::ifstream file;
    file.open(entry.path().string());
    try {
      file.read((char *)&file_number, sizeof(int));
    } catch (...) {
      std::cout << "[ZDB] This file '" << entry.path().string() << "' is probably corrupted."
                << std::endl;
      return false;
    }
    file.close();
    if (file_number != magic_num) return false;
    return true;
  };

  /**
   * Retrieve uuid inside .zdb file.
   */
  static void read_uuid(std::ifstream &file, zdb_uuid_t *uuid) {
    char uuid_c[N_UUID];
    file.ignore(sizeof(int)); // skip magic number
    file.read((char *)&uuid_c, sizeof(char[N_UUID]));
    *uuid = uuid_c;
  };

  /**
   * Retrieve title inside .zdb file.
   */
  static void read_title(std::ifstream &file, zdb_title_t *title) {
    char title_c[N_TITLE];
    file.ignore(sizeof(int));          // skip magic number
    file.ignore(sizeof(char[N_UUID])); // skip uuid
    file.read((char *)&title_c, sizeof(char[N_TITLE]));
    *title = title_c;
  };

  /**
   * Duplicate a Table.
   */
  static ZDB duplicate(ZDB &original) {
    ZDB duplicate;
    duplicate.uuid = generate_uuid();
    duplicate.title = original.get_title();
    duplicate.headers = original.get_headers();
    duplicate.entries = original.get_entries();
    duplicate.state = fresh;
    return duplicate;
  };
};

/**
 * Package of a zdb filename or path and the title it contains.
 */
struct StoredZDB {
  std::string dbfile; // filename of dbfile not loaded
  std::string title;  // title of dbfile
};

/**
 * Package of a Table object, its corresponding filename or path and a visibility bool.
 */
struct LoadedZDB {
  std::string dbfile; // filename of loaded Table
  ZDB table;          // loaded Table
  bool visible;       // is this visible
};

/**
 * Find wrapped zdb Table in a vector by its uuid.
 */
static size_t find(zdb_uuid_t uuid_term, std::vector<LoadedZDB> *vector) {
  for (size_t i{0}; i < vector->size(); ++i)
    if (vector->at(i).table.get_uuid() == uuid_term) return i;
  throw "[ZDB] Unable to find a zdb Table with this uuid!";
};
#endif // ZDB_HEADER_LIBRARY
